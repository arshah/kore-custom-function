---

## Flow for adding/creating/editing bot messages.

You’ll start by adding the bot messages to google sheet. Incase you would like to add the bot messages in the Kora bot builder itself, please go to README2.md

1. Visit this link to access the Google Sheet. -> http://bit.ly/2MErDfY
2. Let us say you are editing an entity node in the Schedule Meeting Dialog Task.
3. Check if the entity node is named correctly. *Please make the entity name more readable and understandable*
4. After saving the changes, copy the entity node name and go to the sheet.
5. You will be able to see a 'Template' sheet. Right click on it and select 'Duplicate'
6. Rename the sheet as the entity name that you copied.
7. After renaming, add the suitable/appropriate bot messages for each default personality. 
8. Do not forget to add messages for Voice Response. 
9. Confirm that you are adhering to the style guide of the messages. 
10. Adding some bling/jazz to the messages is greatly appreciate.
11. After adding all the messages, think if a new personality will be required for the same.
12. If you need a new personality, Please contact Andy. 
13. On the top right of the Menubar, go to Options -> JSON -> Export JSON
14. Wait until the script finishes.. Maybe catch some z's.
15. After the script has finished, this repos KoraDev_botfunction.js should be updated. 
16. The folder is located at -> http://bit.ly/2UlBbiG 
17. A good devOps tip would be that you use Backup and Sync to automatically sync the file on your local computer.
18. Please contact Aakash to set this up.
19. If you are really into these things, you would commit the new updated JSON file on bitbucket for version management.
20. The commands are damn simple. 
21. git add -A | git commit -m "Message about the change" | git push origin master
22. This will help us revert to any previous version.
23. That said, time to upload this file on Kora - bot builder platform under -> Settings -> Advanced Settings
24. Import New -> Add this file KoraDev_botfunction.js
25. Save Changes & Maybe Publish if you like.
26. You are done, your changes are published now. 

## Flow for using bot messages in nodes.

1. There is a code snippet that you can copy and paste in Schedule Meeting -> Email Invitees.
2. This is reduced to very less complex code and simple english functions. 



