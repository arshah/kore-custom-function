// ================= Personality based messages ======================

var typeOfPersonality = context.session.BotUserSession.currentPersonality;
var entityName = context.currentNodeName;
var txtResponses = [];
var voiceResponses = [];
var responses = {};

if(ifNodeExists(entityName)){
    if(!typeOfPersonality){
        typeOfPersonality ="neutral";
    } 
    responses = getMessagesForNodeAndPersonality(entityName,typeOfPersonality);
    
    if(!isEmpty(responses)){
        txtResponses = responses.text;
        voiceResponses = responses.voice;
    }else{
        
    }
}else{
    
}

var ackmsg = intentAck(typeOfPersonality);
var txtmsg = ackmsg + randomMessage(txtResponses);
var voicemsg = ackmsg + randomMessage(voiceResponses);

// ================= Personality based messages ends ======================



// ================= Form Message ======================

//Modify the payload and construct the final message JSON to send to the client as the response.
var message = {
    "type": "template",
    "payload": {
   	        "text": "I am on it! Who would you like to invite to this meeting?",
    		"speech_hint": "I am on it! Who would you like to invite to this meeting?"
    }
};

message.payload.text=txtmsg;
message.payload.speech_hint=voicemsg;

//If SDK, send special JSON
print(JSON.stringify(message));
//If other channel, send plain text.
//print(txtmsg);

// ================= Form Message ends ======================