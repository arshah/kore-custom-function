var json_data = {"confirmInvitees":{"casual":{"text":{"s":["I've got %1 as the invitees.","I've added %1 to the list of invitees."],"p":[]},"voice":{"s":[],"p":[]}},"neutral":{"text":{"s":["%1 the invitees I have got for this meeting.","I've added %1 to the list of invitees."],"p":[]},"voice":{"s":["%1 the invitees I have got for this meeting.","I've added %1 to the list of invitees."],"p":[]}},"professional":{"text":{"s":[],"p":[]},"voice":{"s":[],"p":[]}}},"NoResolvePerson":{"casual":{"text":{"s":["Uh oh, I don't know that person 😳. Please try again…","Well, this is embarassing 😳… I cannot seem to find this person Could you try again?"],"p":[]},"voice":{"s":["Please try again.","Could you try again?"],"p":[]}},"neutral":{"text":{"s":["Sorry, I can't seem to find this person. Please try again.","Sorry, I was not able to find this person. Please try again."],"p":[]},"voice":{"s":["I can't seem to find this person.","I was not able to find this person."],"p":[]}},"professional":{"text":{"s":["Please try again. I am unable to find the person you requested.","I am unable to find the person you are looking for. Please try again."],"p":[]},"voice":{"s":["I am unable to find the person you requested.","I am unable to find the person you are looking for."],"p":[]}}},"Fallback":{"casual":{"text":{"s":["Oops! Something went wrong.","Oops! Something is not right."],"p":[]},"voice":{"s":[],"p":[]}},"neutral":{"text":{"s":["Oops! Something went wrong.","Oops! Something is not right."],"p":[]},"voice":{"s":[],"p":[]}},"professional":{"text":{"s":["Oops! Something went wrong.","Please hang in there. There is a problem on my side."],"p":[]},"voice":{"s":[],"p":[]}}},"EmailInvitees":{"casual":{"text":{"s":["Who should I invite to your meeting? 🤔","Who should attend this meeting? 🧐"],"p":[]},"voice":{"s":["Who should I invite?","Who should attend this meeting?"],"p":[]}},"neutral":{"text":{"s":["Who would you like to invite to this meeting?","Who would you like to invite?","Please provide the names of the invitees..","Who should attend this meeting? 🧐"],"p":[]},"voice":{"s":["Who would you like to invite to this meeting?","Who would you like to invite?"],"p":[]}},"professional":{"text":{"s":["Please provide the names of the invitees…","Who would you like to invite?"],"p":[]},"voice":{"s":["Please provide the names of the invitees","Who would you like to invite?"],"p":[]}}},"person":{"casual":{"text":{"s":["Who should I invite to your awesome meeting?","Who should attend this meeting?"],"p":[]},"voice":{"s":["Who should I invite?","Who should attend this meeting?"],"p":[]}},"neutral":{"text":{"s":["Who would you like to invite to this meeting?","Give me the names of the invitees…"],"p":[]},"voice":{"s":["Who would you like to invite to this meeting?","Give me the names of the invitees"],"p":[]}},"professional":{"text":{"s":["Please provide the names of the invitees…","Who would you like to invite?"],"p":[]},"voice":{"s":["Please provide the names of the invitees","Who would you like to invite?"],"p":[]}}},"IntentAck":{"casual":{"text":{"s":["😀 OK.","Got it! 👍 ,","Alright, sure.","Yup, OK.","You got it!","😎 Cool!","Awesome!","I can help with that.","Not a problem."],"p":[]},"voice":{"s":["Yup, OK.","You got it!"],"p":[]}},"neutral":{"text":{"s":["OK.","Yes, sure.","I'm on it. ","Got it.","Sure."],"p":[]},"voice":{"s":["Yes, sure.","I'm on it. "],"p":[]}},"professional":{"text":{"s":["Okay.","Of course.","I can assist with that.","Certainly.","Absolutely.","Okay, got it!","Yes, got it."],"p":[]},"voice":{"s":["I can assist with that.","Certainly."],"p":[]}}},"GeneralAck":{"casual":{"text":{"s":["😀 OK.","Got it! 👍 ","👍","OK.","Alright!","😎"],"p":[]},"voice":{"s":["OK.","Alright!"],"p":[]}},"neutral":{"text":{"s":["OK.","Got it!","Got that.","Alright!"],"p":[]},"voice":{"s":["OK.","Got it!","Got that.","Alright!"],"p":[]}},"professional":{"text":{"s":["Okay.","Got it.","Alright."],"p":[]},"voice":{"s":["Okay.","Got it.","Alright."],"p":[]}}},"BeforeResp":{"casual":{"text":{"s":["👍 I found %1 articles that might have the info you are looking for...","Here are a few articles that might contain the information...","I found one article.","Here is your article."],"p":[]},"voice":{"s":["I found a few articles.","Here are a few articles.","I found one article.","Here is your article."],"p":[]}},"neutral":{"text":{"s":["👍 I found %1 articles that might have the info you are looking for...","Here are a few articles that might contain the information..."],"p":[]},"voice":{"s":["I found a few articles.","Here are a few articles."],"p":[]}},"professional":{"text":{"s":["👍 I found %1 articles that might have the info you are looking for...","Here are a few articles that might contain the information..."],"p":[]},"voice":{"s":["I found a few articles.","Here are a few articles."],"p":[]}}}};

/* This script file contains some general functions that can be used universally across the bot.
Author: Aakash Shah
Last Edited: Thu, 31-Jan-2019 at 16:05:12 EST
*/

//======================================== BEGIN GLOBAL VARIABLES ============================================
var url = "https://drive.google.com/uc?export=view&id=1NVHNlg_jbuzUX1rtHSZ62bFK8gPP8TJJ";
// console.log(json_data.keys());

//This is the list of intent/general acknowledgement by personality that should only be used right after intent is recognized
var casualIntentAckList = json_data.IntentAck.casual.text.s.concat(json_data.IntentAck.casual.text.p);

var casualGeneralAckList = json_data.GeneralAck.casual.text.s.concat(json_data.GeneralAck.casual.text.p);

var neutralIntentAckList = json_data.IntentAck.neutral.text.s.concat(json_data.IntentAck.neutral.text.p);

var neutralGeneralAckList = json_data.GeneralAck.neutral.text.s.concat(json_data.GeneralAck.neutral.text.p);

var professionalIntentAckList = json_data.IntentAck.professional.text.s.concat(json_data.IntentAck.professional.text.p);

var professionalGeneralAckList = json_data.GeneralAck.professional.text.s.concat(json_data.GeneralAck.professional.text.p);

//======================================== END OF GLOBAL VARIABLES ============================================

//Function to get a random number between the ranges.
function rndNum(min, max){
    
    "use strict";
     var num = Math.floor(Math.random() * (max - min + 1)) + min;
     return num;
}
//alert(rndNum(1,5));

//Function to render a simple string array with commas, spacing and an 'and' before the last item.
function renderArrays(arr){
    var text=arr[0];
    if(arr.length>1){
        for(var i=1; i<arr.length-1; ++i) {
            text= text + ", " + arr[i];
        }
        text = text + " and " + arr[arr.length-1];
    }
    return text;
}


//Function to randomly get an intent acknowledgement message.
function intentAck(personality){
    "use strict";
    //Switch based on the personality found, default is neutral incase of error or wrong param.
	var text;
    switch (personality) {
        case "casual":
            text = randomMessage(casualIntentAckList);
            break; 
        case "neutral":
            text = randomMessage(neutralIntentAckList);
            break;
        case "professional":
            text = randomMessage(professionalIntentAckList);
            break; 
        default: 
            text = "OK.";
    }

    return text + " ";
}


function generalAck(personality){

	"use strict";
    //Switch based on the personality found, default is neutral incase of error or wrong param.
	var text;
    switch (personality) {
        case "casual":
            text = randomMessage(casualGeneralAckList);
            break; 
        case "neutral":
            text = randomMessage(neutralGeneralAckList);
            break;
        case "professional":
            text = randomMessage(professionalGeneralAckList);
            break; 
        default: 
            text = "OK.";
    }

    return text + " ";
}
//alert("final text:"+intentAck("professional")) //JavaScript Document

function addQuickReplies(message,contentType,QRTitle,QRPayload){

	"use strict";
	//If no QRValue is given, assume QRLabel as QRValue
	QRPayload = QRPayload !== undefined ? QRPayload : QRTitle;
    
	//Change the message template type to quick replies
    message.payload.template_type="quick_replies";
    message.payload.quick_replies=[];

	//Loop through and create QR objects and add into the message payload.
    for (var i = 0; i < QRTitle.length; i++) {
        var quickReply = {
            "content_type": contentType,
            "title": QRTitle[i],
            "payload":QRPayload[i]
        };
        message.payload.quick_replies.push(quickReply);
    }

    return message;
}


//========================================== BEGIN BOT MESSAGES FUNCTIONS ====================================
/*
Examples:
*/

// console.log(getNodeNames())
// console.log(getPersonalitiesForNode("IntentAck"))
// console.log(getTextMessagesNodeAndPersonality("IntentAck","casual"))
// console.log(getVoiceMessagesNodeAndPersonality("BeforeResp","professional", "p"));
// console.log(randomMessage(getTextMessagesNodeAndPersonality("IntentAck","casual")));
// console.log(getMessagesForNodeAndPersonality("IntentAck","casual"));

function isEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return JSON.stringify(obj) === JSON.stringify({});
}

//alert(randomMessage(["hi","hello","hey"]))
function randomMessage(arr){
    if (arr.length > 1){
        var min = 0;
        var max = arr.length - 1;
        var num = rndNum(min,max);
        return arr[num];
    }else if (arr.length == 1){
        return arr[0];
    }else{
        return "There is a problem on our side. Please hang tight.";
    }
}

// Check if node name exists in json_data
function ifNodeExists(node_name){
    for (let node of Object.keys(json_data)){
        if(node === node_name){
            return true;
        }
    }
    return false;
}

// Returns an array of node names.
function getNodeNames(){
    return Object.keys(json_data);
}

//Check if personality exists in node.
function ifPersonalityExistsInNode(node_name,personality){
   for (let node of Object.keys(json_data)){
        if(node === node_name){
            for (let person of getPersonalitiesForNode(node)){
                if(person === personality){
                    return true;
                }
            }
        }
    }
    
    return false;
}

// Returns an array of personality for specified node name.
function getPersonalitiesForNode(node_name){
    for (let node of Object.keys(json_data)){
        if(node === node_name){
            return Object.keys(json_data[node_name])
        }
    }
    return []
}


function getMessagesForNodeAndPersonality(node_name, personality,plur="all"){
    var txtResponses = [];
    var voiceResponses = [];
    var response = {};

    if(!ifPersonalityExistsInNode(node_name,personality)){
        // console.log(Object.entries(response) === [])
        return response;
    }

    for (let node of Object.keys(json_data)){
        if(node === node_name){
            for (let person of getPersonalitiesForNode(node)){
                if(person === personality){
                    if(plur == "s"){
                       txtResponses = json_data[node_name][personality].text.s
                       voiceResponses = json_data[node_name][personality].voice.s

                    }else if(plur == "p"){
                       txtResponses =  json_data[node_name][personality].text.p;
                       voiceResponses = json_data[node_name][personality].voice.p;

                    }else{
                        txtResponses = json_data[node_name][personality].text.p.concat(json_data[node_name][personality].text.s);
                        voiceResponses = json_data[node_name][personality].voice.p.concat(json_data[node_name][personality].voice.s);
                    }
                    break;
                }
            }
        }
    }

    if(txtResponses.length > 0){
        response["text"] = txtResponses;
    }

    if(voiceResponses.length > 0){
        response["voice"] = voiceResponses;
    }

    return response;
}

function getTextMessagesForNodeAndPersonality(node_name,personality,plur="all"){
   for (let node of Object.keys(json_data)){
        if(node === node_name){
            for (let person of getPersonalitiesForNode(node)){
                if(person === personality){
                    if(plur == "s"){
                       return json_data[node_name][personality].text.s
                    }else if(plur == "p"){
                       return json_data[node_name][personality].text.p;
                    }else{
                        return json_data[node_name][personality].text.p.concat(json_data[node_name][personality].text.s);
                    }
                }
            }
        }
    }
    return []
}

function getVoiceMessagesForNodeAndPersonality(node_name,personality,plur="all"){
   for (let node of Object.keys(json_data)){
        if(node === node_name){
            for (let person of getPersonalitiesForNode(node)){
                if(person === personality){
                    if(plur == "s"){
                        return json_data[node_name][personality].voice.s
                    }else if(plur == "p"){
                        return json_data[node_name][personality].voice.p;
                    }else{
                        return json_data[node_name][personality].voice.p.concat(json_data[node_name][personality].voice.s);
                    }
                }
            }
        }
    }
    return []
}



//========================================== END BOT MESSAGES FUNCTIONS ====================================








